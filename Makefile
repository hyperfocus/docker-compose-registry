registry: registry-compose

registry-compose-prod:
	cd prod/compose && docker-compose up -d

registry-run-prod:
	cd prod/run && bash registry-run.sh

registry-compose-dev:
	cd dev/compose && docker-compose up -d

registry-run-dev:
	cd dev/run && bash registry-run.sh
